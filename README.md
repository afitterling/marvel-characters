Hi there, my name is Alex.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

See the alternative feature branch for the image background scale fix: /features/image-scale-fix

## to run it

checkout following commands:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

tests haven't been implemented, yet.

### `yarn build`

[stay tuned] currently using zeit.co/now

## Accessing the API with python notebook (https://jupyter.org/)

- cd into DIR
- pip install virtualenv
- virtualenv .
- source bin/activate
- pip install -r requirements.txt
- jupyter-notebook
- open file API python.ipynb
- step over it

