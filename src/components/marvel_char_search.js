import React from 'react'
import {Form, Grid} from 'semantic-ui-react'
import MarvelCharAutoComplete from "./marvel_char_autocomplete";

/**
 * the search autocomplete dialog
 *
 * @param onChange
 * @param disabled
 * @param setOffset
 * @param marvelChars
 * @param setQuery
 * @returns {*}
 * @constructor
 */
const MarvelCharSearch = ({onChange, disabled, setOffset, marvelChars, setQuery}) => {

    return (
        <Grid>
            <Grid.Column width={7}>
                <Form>
                    <Form.Field>
                        <label>Hero's name</label>
                        {/*https://react.semantic-ui.com/modules/dropdown/#usage-remote*/}
                        <MarvelCharAutoComplete disabled={disabled} setQuery={setQuery} setOffset={setOffset} onChange={onChange} items={marvelChars} />
                    </Form.Field>
                </Form>
            </Grid.Column>
        </Grid>
    );

}

export default MarvelCharSearch;
