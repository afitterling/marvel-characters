import React from "react";
import MarvelCharacters from "./marvel_characters";
import {Container, Divider, Header, Icon, Segment} from "semantic-ui-react";
import MarvelCharSearch from "./marvel_char_search";
import MarvelPaginator from "./marvel_paginator";
import {debounce} from "lodash";
import {searchWithQueryOrAll} from "../services/marvelServices";

export const MarvelApp = () => {

    /*
        Must not be matched with the debounce of the autocomplete, necessarily. (but would be nice)
        This one not only prevents jittering updates on characters in a whole, but also in case the
        pager is triggered too fast. Generally: only calls to fetch are made if the
        succeeding call is later as the given time in ms.
     */
    const DEBOUNCE_FETCH_CHARACTERS = 250;


    const [searchResults, setSearchResults] = React.useState({
        error: {statusText: null, statusCode: null},
        data: {results: [], total: 0}
    });

    const [offset = 0, setOffset] = React.useState(0);
    const [query, setQuery] = React.useState('');
    const {data: {results: characters, total}} = searchResults;

    const fetchCharacters = React.useCallback(debounce((offset = 0, count = 3, query = '') => {
        setQuery(query);
        // reset offset later in case if query
        return searchWithQueryOrAll(offset, query, count).then(setSearchResults);
    }, DEBOUNCE_FETCH_CHARACTERS), []);

    /**
     * watch the pagination and retrieve new entities upon change
     */
    React.useEffect(() => {
        fetchCharacters(offset, 3, query);
    }, [offset]);

    /**
     * have a watch on the search query, if changed also reset offset to 0
     */
    React.useEffect(() => {
        setOffset(0)
        fetchCharacters(offset, 3, query);
    }, [query]);

    return (
        <Container>
            <Header as='h1'>Marvel App</Header>
            <MarvelCharSearch disabled={searchResults.error} setQuery={setQuery} setOffset={setOffset} onChange={fetchCharacters}/>
            <Divider/>
            {
                // display an error if occurred any
                searchResults.error && searchResults.error.statusText ? <Segment placeholder>
                        <Header icon>
                            <Icon name='heartbeat'  circular color='red'/>
                            An Error occured: {searchResults.error.statusText}
                        </Header>
                    </Segment>
                    : null
            }
            <MarvelCharacters characters={characters}> </MarvelCharacters>
            <Divider/>
            <MarvelPaginator total={total} onChangePaginatorState={setOffset} offset={offset}
                             pageSize={3}></MarvelPaginator>
        </Container>
    );

}
