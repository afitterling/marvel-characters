import React from "react";
import {Card, Grid, GridColumn, Image} from "semantic-ui-react";
import moment from 'moment';
import MarvelCharDetail from "./marvel_char_detail";

/**
 * displays the marvel characters in columns x 3 (if there are any)
 *
 * @param characters
 * @returns {*}
 * @constructor
 */
const MarvelCharacters = ({characters = []}) => {
    const marvelCardStyle = {
        width: "auto",
        height: "450px"
    }

    return (
        <Grid columns='3'>
            {
                characters.map((marvelChar) =>
                    <GridColumn>
                        <Card style={marvelCardStyle}>
                            <Image src={`${marvelChar.thumbnail.path}.${marvelChar.thumbnail.extension}`} wrapped
                                           ui={false}/>
                            <Card.Content>
                                <Card.Header>{marvelChar.name}</Card.Header>
                                <Card.Meta>
                                    <span
                                        className='date'>updated {moment(marvelChar.modified).format('DD.MM.YYYY HH:mm')}</span>
                                </Card.Meta>
                                <MarvelCharDetail marvelChar={marvelChar}/>
                                <Card.Description>
                                    {marvelChar.description.length > 40 ? marvelChar.description.substring(0, 40 - 3) + '...' : marvelChar.description}
                                </Card.Description>
                            </Card.Content>
                        </Card>
                    </GridColumn>
                )
            }
        </Grid>
    );
}

export default MarvelCharacters;
