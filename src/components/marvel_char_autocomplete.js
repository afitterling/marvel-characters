import React from 'react'
import {Search} from 'semantic-ui-react'
import {searchWithQueryOrAll} from "../services/marvelServices";
import {debounce} from "lodash";

/**
 * An AutoComplete Field with Semantic UI (remotely controlled)
 *
 * @param value
 * @param onChange
 * @param setQuery propagates state to parent (wich uses it to retrieve the list)
 * @param disabled
 * @returns {*}
 * @constructor
 */
const MarvelCharAutoComplete = ({value, onChange, setQuery}) => {

    const AUTOCOMPLETE_MAX = 10;
    const DEBOUNCE_AUTO_COMPLETE = 250; // ms to make the autocomplete a user friendly feeling (no jittering if fast typing)

    const [options, setOptions] = React.useState([]);

    const transformToOptionsList = (r) => {
        setOptions(
            r.data.results.map( c => {
                return {title: c.name};
            })
        );
    }

    const fetchAutoCompleteItems = (searchQuery='') => {
        searchWithQueryOrAll(0, searchQuery, AUTOCOMPLETE_MAX).then(transformToOptionsList);
    }

    /**
     * onSearchPattern change calls update Auto Complete and propagates searchQuery to parent component
     * @type {Function}
     */
    const onSearchChange = React.useCallback(debounce((e, {value}) => {
        // propagate the entities query back to the parent to reflect the list results (acording to pagination)
        setQuery(value);
        fetchAutoCompleteItems(value);
    }, DEBOUNCE_AUTO_COMPLETE),[])

    /**
     * simply calls the onChange passed down to this component with a specific search String that should match exactly one character
     * selected by the user in dropdown
     * @type {Function}
     */
    const onChangeFn = React.useCallback((e, {result: { title }}) => {
        onChange(0, 1, title);
    })

    return (
        <Search
            loading={false}
            placeholder='Select Marvel Hero'
            onResultSelect={onChangeFn}
            onSearchChange={onSearchChange}
            results={options}
            value={value}
        />
    );
}

export default MarvelCharAutoComplete
