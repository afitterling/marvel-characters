import React from 'react';
import {Button, Grid, Icon} from "semantic-ui-react";

/**
 * a simple paginator written by my own
 * uses ReactHooks (Callbacks) to propagate the offset to the parent component
 *
 * @param offset
 * @param onChangePaginatorState
 * @param pageSize
 * @param total
 * @returns {*}
 * @constructor
 */
const MarvelPaginator = ({offset, onChangePaginatorState, pageSize, total}) => {

    /**
     * increase offset
     */
    const onClickPageUp = () => {
        offset = offset < total - pageSize ? offset + pageSize: offset;
        onChangePaginatorState(offset);
    };

    /**
     * this function acts in decreasing the page count (literally the offset)
     */
    const onClickPageDown = () => {
        offset = offset >= pageSize ? offset - pageSize : offset;
        onChangePaginatorState(offset);
    };

    /**
     * this function returns the number of total available pages and ensures that at least 1 is being showed to the user
     * @param fnTotal
     * @returns {number}
     */
    const totalPages = (fnTotal = Math.floor(total / 3)) => {
        return total % pageSize !== 0 || total === 0 ? fnTotal + 1 : fnTotal;
    }

    return (
        <Grid centered columns={3}>
            <Grid.Column>
                <Button.Group>
                    <Button disabled={offset === 0} onClick={onClickPageDown}><Icon name='left arrow'/></Button>
                    <Button>{offset / 3 + 1} / {totalPages()}</Button>
                    <Button disabled={offset + pageSize >= total} onClick={onClickPageUp}>
                        <Icon name='right arrow'/></Button>
                </Button.Group>
            </Grid.Column>
        </Grid>
    );
}

export default MarvelPaginator;
