
import React from 'react'
import {Button, Divider, Grid, GridColumn, Header, Icon, Image, List, Modal} from 'semantic-ui-react'

/**
 * displays the marvel character's details
 *
 * @param marvelChar
 * @returns {*}
 * @constructor
 */
const MarvelCharDetail = ({marvelChar}) => (
    <Modal trigger={<Button>See More</Button>}>
        <Modal.Header>{marvelChar.name}</Modal.Header>
        <Modal.Content image>
            <Grid>
                <GridColumn centered width={6}>
                    <Image wrapped size='medium' src={`${marvelChar.thumbnail.path}.${marvelChar.thumbnail.extension}`} />

{/*
                    <Button centered>
                        <Icon color='red' size='large' name='heart' />
                        <span>Add To Favorites</span>
                    </Button>
*/}

                </GridColumn>
                <GridColumn width={8}>
                    <Modal.Description>
                        <Header as='h1'>Details</Header>
                        <p>
                            { marvelChar.description }
                        </p>

                        <Header>Series</Header>
                        <List>
                        { [...marvelChar.series.items].slice(0,5).map((item) => {
                            return (
                                <List.Item>
                                    <List.Header>{item.name}</List.Header>
                                </List.Item>
                            );
                            }
                        )}
                        </List>

                        <Header>Stories</Header>
                        <List>
                            { [...marvelChar.stories.items].slice(0,5).map((item) => {
                                    return (
                                        <List.Item>
                                            <List.Header>{item.name}</List.Header>
                                        </List.Item>
                                    );
                                }
                            )}
                        </List>

                        <Header>Comics</Header>
                        <List>
                            { [...marvelChar.comics.items].slice(0,5).map((item) => {
                                    return (
                                        <List.Item>
                                            <List.Header>{item.name}</List.Header>
                                        </List.Item>
                                    );
                                }
                            )}
                        </List>

                    </Modal.Description>

                    <Divider/>
                </GridColumn>
            </Grid>
        </Modal.Content>
    </Modal>
)

export default MarvelCharDetail;
