import React from 'react';
import './App.css';
import {MarvelApp} from "./components/marvel_app";

function App() {

  return (
    <MarvelApp>
    </MarvelApp>
  );
}

export default App;
