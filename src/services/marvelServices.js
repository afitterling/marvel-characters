/**
 * a simple recover function
 *
 * @param r
 * @returns {{data: {total: number, results: Array}, error: {statusText: (*|null|string), statusCode: *}}|never|*|Promise<any>}
 */

export const recoverWithError = (r) => {
    if (r.ok) {
        return r.json()
    }
    console.error("error", r.statusCode, r.statusText);
    return {error: {statusCode: r.status, statusText: r.statusText}, data: {results: [], total: 0}};
}

/**
 * searches for marvel characters starting with string {query}
 *
 * @param offset
 * @param query
 * @param count
 * @returns {Promise<Response | never>}
 */
export const searchCharactersNameStartsWith = (offset=0, query='', count=3) => {
    const urlNameQuery = `https://gateway.marvel.com/v1/public/characters?nameStartsWith=${query}&offset=${offset}&limit=${count}&ts=1565922410&apikey=6a038473ffd6407750a2ea27115f7e7c&hash=1492df65a88ef98a1a279719fe509f72`;
    return fetch(urlNameQuery).then(recoverWithError);
}

/**
 * gives a list of all characters
 * @param offset
 * @param count
 * @returns {Promise<Response | never>}
 */
export const searchAllCharacters = (offset=0, count=3) => {
    const urlAll = `https://gateway.marvel.com/v1/public/characters?&offset=${offset}&limit=${count}&ts=1565922410&apikey=6a038473ffd6407750a2ea27115f7e7c&hash=1492df65a88ef98a1a279719fe509f72`;
    return fetch(urlAll).then(recoverWithError);
}

/**
 * either return a list of all characters or call the startsWithName service function
 *
 * @param offset
 * @param query
 * @param count
 * @returns {*}
 */
export const searchWithQueryOrAll = (offset=0, query='', count=3) => {
    return !!query ? searchCharactersNameStartsWith(offset, query, count) : searchAllCharacters(offset, count).catch(recoverWithError);
}

